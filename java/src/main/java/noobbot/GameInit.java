package noobbot;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class GameInit {
	// JSON Data
	private String msgType;
	private Data data;
	private static List<Lane> lanes;

	private static final int LOOK_AHEAD_LENGTH = 150;
	private static int currentPiecePos = 0;
	private static int numPieces = 0;
    private static boolean turboAvailable = false;

	public List<Piece> lookAheadPieces(CarPositions carPosition) {
		// Determine how many pieces span the distance of LOOK_AHEAD_LENGTH

		double distance = data.getPieces().get(currentPiecePos).getLength()
				- carPosition.getOurCarData().getPiecePosition()
						.getInPieceDistance();
		int i;
		for (i = 1; distance < LOOK_AHEAD_LENGTH; i++) {
			if (currentPiecePos + i >= numPieces)
				break;
			distance += data.getPieces().get(currentPiecePos + i).getLength();
		}
		if (currentPiecePos + i >= numPieces) {
			return data.getPieces().subList(currentPiecePos,
					currentPiecePos + (numPieces - currentPiecePos - 1));
		}
		return data.getPieces().subList(currentPiecePos, currentPiecePos + i);
	}

	public static void nextPiecePosition() {
		if (currentPiecePos == numPieces - 1) {
			currentPiecePos = 0;
		} else {
			currentPiecePos++;
		}
	}

	public boolean isCarOnSwitch() {
		return data.getPieces().get(currentPiecePos).isSwitch();
	}

    public void setTurboAvailable() {
        turboAvailable = true;
    }

    public static void consumeTurbo() {
        turboAvailable = false;
    }

    public static boolean isTurboAvailable() {
        return turboAvailable;
    }

	public void init() {
        currentPiecePos = 0;
		numPieces = data.getPieces().size();
		lanes = data.getLanes();
        ThrottleCalculator.once = true;
        CarPositions.firstTime = true;
	}

	// Getters
	/*
	 * Parameter is piece index Returns: 0 if straight, 1 if switch, 2 if curve
	 */
	public int getPieceType(int x) {
		return data.getPieceType(x);
	}

	/*
	 * Parameter is piece index for following 3 methods
	 */
	public double getPieceLength(int x) {
		return data.getPieceLength(x);
	}

	public double getRadius(int x) {
		return data.getRadius(x);
	}

	public double getAngle(int x) {
		return data.getAngle(x);
	}

	public static List<Lane> getLanes() {
		return lanes;
	}

	/*
	 * Returns starting position as so [x, y, angle]
	 */
	public double[] getStartPosition() {
		return data.getStartPosition();
	}

	/*
	 * Returns car dimensions as so [length, width]
	 */
	public double[] getCarDimensions() {
		return data.getCarDimensions();
	}

	public double getGuideFlagPosition() {
		return data.getGuideFlagPosition();
	}

	public List<Piece> getPieces() {
		return data.getPieces();
	}

	// Embedded Classes
	public class Data {
		private Race race;

		// Getters
		public int getPieceType(int x) {
			return race.getPieceType(x);
		}

		public List<Piece> getPieces() {
			return race.getPieces();
		}

		public double getPieceLength(int x) {
			return race.getPieceLength(x);
		}

		public double getRadius(int x) {
			return race.getRadius(x);
		}

		public double getAngle(int x) {
			return race.getAngle(x);
		}

		public List<Lane> getLanes() {
			return race.getLanes();
		}

		public double[] getStartPosition() {
			return race.getStartPosition();
		}

		public double[] getCarDimensions() {
			return race.getCarDimensions();
		}

		public double getGuideFlagPosition() {
			return race.getGuideFlagPosition();
		}
	}

	public static class Race {
		private Track track;
		private List<Cars> cars;
		private RaceSession raceSession;

		// Track Getters
		public List<Piece> getPieces() {
			return track.getPieces();
		}

		public int getPieceType(int x) {
			return track.getPieceType(x);
		}

		public double getPieceLength(int x) {
			return track.getLength(x);
		}

		public double getRadius(int x) {
			return track.getRadius(x);
		}

		public double getAngle(int x) {
			return track.getAngle(x);
		}

		public List<Lane> getLanes() {
			return track.getLanes();
		}

		public double[] getStartPosition() {
			return track.getPosition();
		}

		// Car Getters
		public double[] getCarDimensions() {
			return cars.get(0).getDimensions();
		}

		public double getGuideFlagPosition() {
			return cars.get(0).getGuideFlagPosition();
		}
	}

	public static class Track {
		private String id;
		private String name;
		private List<Piece> pieces;
		private List<Lane> lanes;
		private StartingPoint startingpoint;

		// Piece Getters
		public int getPieceType(int x) {
			return pieces.get(x).getPieceType();
		}

		public double getLength(int x) {
			return pieces.get(x).getLength();
		}

		public double getRadius(int x) {
			return pieces.get(x).getRadius();
		}

		public double getAngle(int x) {
			return pieces.get(x).getAngle();
		}

		public List<Piece> getPieces() {
			return pieces;
		}

		// Lane Getters
		public List<Lane> getLanes() {
			return lanes;
		}

		// Start Point Getters
		public double[] getPosition() {
			return startingpoint.getPosition();
		}
	}

	public static class Lane {
		private int distanceFromCenter;
		private int index;

		// Getters
		public int getDistanceFromCenter() {
			return distanceFromCenter;
		}

		public int getIndex() {
			return index;
		}
	}

	public static class StartingPoint {
		private Position position;
		private double angle;

		// Getters
		public double[] getPosition() {
			double[] pos = { position.getX(), position.getY(), angle };
			return pos;
		}
	}

	public static class Position {
		private double x;
		private double y;

		// Getters
		public double getX() {
			return x;
		}

		public double getY() {
			return y;
		}
	}

	public static class Cars {
		private ID id;
		private Dimensions dimensions;

		// Getters
		public double[] getDimensions() {
			double[] dim = { dimensions.getLength(), dimensions.getWidth() };
			return dim;
		}

		public double getGuideFlagPosition() {
			return dimensions.getGuideFlagPosition();
		}
	}

	public static class ID {
		private String name;
		private String color;
	}

	public static class Dimensions {
		private double length;
		private double width;
		private double guideFlagPosition;

		// Getters
		public double getLength() {
			return length;
		}

		public double getWidth() {
			return width;
		}

		public double getGuideFlagPosition() {
			return guideFlagPosition;
		}
	}

	public static class RaceSession {
		private int laps;
		private int maxLapTimeMs;
		private boolean quickRace;
	}

}
