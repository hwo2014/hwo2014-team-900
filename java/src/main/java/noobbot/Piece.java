package noobbot;

import com.google.gson.annotations.SerializedName;

public class Piece {
    public static final int CURVE_PIECE = 2;
    public static final int SWITCH_PIECE = 1;
    public static final int STRAIGHT_PIECE = 0;

	private double length;
	@SerializedName("switch")
	private boolean switch_p = false;
	private double radius = -1;
	private double angle;
	
	//Getters
	public int getPieceType() {
        if (radius != -1){
        	return CURVE_PIECE;
        } else if (switch_p){
        	return SWITCH_PIECE;
        } else {
        	return STRAIGHT_PIECE;
        }
    }
	public boolean isSwitch(){
		return switch_p;
	}
	public double getLength() {
		return length;
	}
	public double getRadius() {
		return radius;
	}
	public double getAngle() {
		return angle;
	}
	
}
