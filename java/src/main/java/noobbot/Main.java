package noobbot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

public class Main {
    public static void main(String... args) throws IOException {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];

        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        final Socket socket = new Socket(host, port);
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

        new Main(reader, writer, new Join(botName, botKey));
    }

    final Gson gson = new Gson();
    private PrintWriter writer;
    private GameInit gameInit;

    public Main(final BufferedReader reader, final PrintWriter writer, final Join join) throws IOException {
        this.writer = writer;
        String line = null;

        send(join);

        while((line = reader.readLine()) != null) {
            final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);
            if (msgFromServer.msgType.equals("carPositions")) {
                CarPositions carPositions = gson.fromJson(line, CarPositions.class);
                carPositions.init();
                List<Piece> upcomingPieces = gameInit.lookAheadPieces(carPositions);
                if(gameInit.isCarOnSwitch() && Switcher.checker(upcomingPieces, carPositions, gameInit)) {
                    send(new SwitchLane(Switcher.getDirection()));
                }
                if(ThrottleCalculator.shouldTurbo()) {
                    send(new Turbo());
                }
                else {
                    float throttle = ThrottleCalculator.calculate(gameInit.lookAheadPieces(carPositions), carPositions);
                    send(new Throttle(Float.isNaN(throttle) ? 0.5f : throttle));
                }
            } else if (msgFromServer.msgType.equals("join")) {
                System.out.println("Joined");
            } else if (msgFromServer.msgType.equals("gameInit")) {
                gameInit = gson.fromJson(line, GameInit.class);
                gameInit.init();
                System.out.println("Race init");
            } else if (msgFromServer.msgType.equals("gameEnd")) {
                System.out.println("Race end");
            } else if (msgFromServer.msgType.equals("gameStart")) {
                System.out.println("Race start");
            } else if (msgFromServer.msgType.equals("crash")) {
                //TODO do we need to do anything when crash happens?
            } else if (msgFromServer.msgType.equals("tournamentEnd")) {
                break;
            } else if (msgFromServer.msgType.equals("turboAvailable")) {
                //gameInit.setTurboAvailable();
            } else {
                //Output the msgType if unexpected
                System.out.println("Unexpected Msg Type: " + msgFromServer.msgType);
                System.out.println("Unexpected Msg Data: " + line);
                send(new Ping());
            }
        }
    }

    private void send(final SendMsg msg) {
        writer.println(msg.toJson());
        writer.flush();
    }
}

abstract class SendMsg {
    public String toJson() {
        return new Gson().toJson(new MsgWrapper(this));
    }

    protected Object msgData() {
        return this;
    }

    protected abstract String msgType();
}

class MsgWrapper {
    public final String msgType;
    public final Object data;

    MsgWrapper(final String msgType, final Object data) {
        this.msgType = msgType;
        this.data = data;
    }

    public MsgWrapper(final SendMsg sendMsg) {
        this(sendMsg.msgType(), sendMsg.msgData());
    }
}

class Join extends SendMsg {
    public final String name;
    public final String key;
    private static final String JOIN = "join";
    private static final String JOIN_RACE = "joinRace";
    private static final String CREATE_RACE = "createRace";
    private final String trackName = "usa";

    Join(final String name, final String key) {
        this.name = name;
        this.key = key;
    }

    @Override
    protected String msgType() {
        return JOIN;
    }

    @Override
    protected Object msgData() {
        if(msgType() == JOIN) {
            return super.msgData();
        } else {
            JsonObject jo = new JsonObject();
            JsonObject bot = new JsonObject();
            bot.addProperty("name", "doomstarks"); 
            bot.addProperty("key", "PSVlfAEJZ3ep0g");
            jo.add("botId", bot);
            jo.addProperty("trackName", trackName);
            jo.addProperty("carCount", 1);
            return jo;
        }
    }
}

class Ping extends SendMsg {
    private static final String PING = "ping";
    @Override
    protected String msgType() {
        return PING;
    }
}

class Throttle extends SendMsg {
    private static final String THROTTLE = "throttle";
    private double value;

    public Throttle(double value) {
        this.value = value;
    }

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return THROTTLE;
    }
}

class Turbo extends SendMsg {
    private static final String TURBO = "turbo";

    @Override
    protected Object msgData() {
        return "More Turbo Pls";
    }

    @Override
    protected String msgType() {
        return TURBO;
    }
}

class SwitchLane extends SendMsg {
    public static final String LEFT = "Left";
    public static final String RIGHT = "Right";
    private static final String msgType = "switchLane";
    
    private String direction;

    public SwitchLane(String direction){
        this.direction = direction;
    }

    @Override
    protected Object msgData() {
        return direction;
    }

    @Override
    protected String msgType() {
        return msgType;
    }
}
