package noobbot;

import java.util.ArrayList;
import java.util.List;
import noobbot.CarPositions.Data;
import noobbot.CarPositions;
import noobbot.GameInit.Lane;

public class Switcher {
	
	private static String direction;
	public static final int CURR_I = 0;
	public static final int NEXT_I = 1;

	public static boolean checker(List<Piece> pieces, CarPositions carPosition, GameInit gi){
		Data car = carPosition.getOurCarData();
		Piece curr_p = pieces.get(CURR_I);
		Piece next_p = pieces.get(NEXT_I);
		
		List<Lane> lanes = gi.getLanes();
		if (curr_p.isSwitch()){
			if((next_p.getAngle()>0) && (car.getPiecePosition().getLane().getStartLaneIndex() != lanes.size()-1)){
				//&& (car.getPiecePosition().getLane().getStartLaneIndex() != lanes.size()-1)
				direction = "Right";
				return true;
			}else if ((next_p.getAngle()<0) && (car.getPiecePosition().getLane().getStartLaneIndex() != 0)){
				//&& (car.getPiecePosition().getLane().getStartLaneIndex() != 0)
				direction = "Left";
				return true;
			}else{
				return false;
			}
		}
		return false;
	}
	
	public static String getDirection(){
		return direction;
	}

}
